export default {
	currentRep({ workout }) {
		// find unfinished day
		const dayIndex = workout.days.findIndex((day) => !day.state);
		if (dayIndex < 0) {
			return null;
		}

		const day = workout.days[dayIndex],
			setIndex = day.sets.findIndex((set) => !set.state),
			set = day.sets[setIndex],
			repIndex = set.reps.findIndex((rep) => !rep);

		return { dayIndex, setIndex, repIndex };
	},
	previousRep({ workout }, getters) {
		const currentRep = getters.currentRep;

		if (!currentRep) {
			return null;
		}

		let { dayIndex, setIndex, repIndex } = currentRep;

		if (repIndex > 0) {
			repIndex--;
		} else if (setIndex > 0) {
			// wrap around to previous set
			setIndex--;
			repIndex = workout.days[dayIndex].sets[setIndex].reps.length - 1;
		} else if (dayIndex > 0) {
			// wrap around to previous day
			dayIndex--;
			setIndex = workout.days[dayIndex].sets.length - 1;
			repIndex = workout.days[dayIndex].sets[setIndex].reps.length - 1;
		} else {
			return null;
		}

		return { dayIndex, setIndex, repIndex };
	},
};
