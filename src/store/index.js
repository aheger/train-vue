import Vuex from 'vuex';
import Vue from 'vue';

import DefaultProgram from './defaultProgram';
import DefaultLifts from './defaultLifts';

import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import { createWorkoutState } from './helper';

Vue.use(Vuex);

export default new Vuex.Store({
	strict: true,
	state: {
		dayIndex: 0,
		isLoading: 0,
		isSaving: false,
		lifts: DefaultLifts,
		program: DefaultProgram,
		restTimer: null,
		userId: null,
		workout: createWorkoutState(DefaultProgram),
	},
	mutations,
	actions,
	getters,
});
