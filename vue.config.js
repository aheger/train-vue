const isProd = process.env.NODE_ENV === 'production';
const CompressionWebpackPlugin = require('compression-webpack-plugin');

const publicPath = process.env.TRAIN_VUE_BASE_URL || '/';

module.exports = {
	publicPath,
	productionSourceMap: false,
	configureWebpack: (config) => {
		if (isProd) {
			config.plugins.push(new CompressionWebpackPlugin());
		}
	},
};
