export const repStates = {
	REP_STATE_NONE: 0,
	REP_STATE_DONE: 1,
	REP_STATE_FAILED: 2,
};

// types of how to progress a lift after a finished workout period
export const progTypes = {
	// increase by fixed weight
	PROG_TYPE_FIXED: 0,
	// increase by ratio
	PROG_TYPE_RATIO: 1,
};

export const REST_INTERVAL = 60;
