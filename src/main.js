import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import lang from './lang';

import './main.css';
// import loader css so it can be displayed before app starts
import './loader.css';
import './ui.css';
import './icons.css';
import './animation.css';

import IconButton from './components/IconButton';

Vue.config.productionTip = false;

Vue.mixin({
	components: {
		IconButton,
	},
	methods: {
		lang,
	},
});

router.beforeEach((to, _from, next) => {
	if (!store.state.userId || store.state.userId === to.query.userId) {
		return next();
	}

	// inject user id to query
	const query = {
		...to.query,
		userId: store.state.userId,
	};

	next({
		...to,
		query
	})
});

new Vue({
	el: '#app',
	router,
	store,
	render: (h) => h(App),
});
