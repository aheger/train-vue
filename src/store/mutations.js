import { getResultState } from './helper';

export default {
	dayIndex(state, newIndex) {
		const dayCount = state.program.days.length;

		// wrap around on invalid index
		if (newIndex < 0) {
			newIndex = dayCount - 1;
		} else if (newIndex >= dayCount) {
			newIndex = 0;
		}

		state.dayIndex = newIndex;
	},
	isSaving(state, newVal) {
		state.isSaving = newVal;
	},
	lifts(state, newLifts) {
		state.lifts = Object.assign({}, state.lifts, newLifts);
	},
	pushLoad(state) {
		state.isLoading++;
	},
	popLoad(state) {
		if (state.isLoading === 0) {
			return;
		}
		state.isLoading--;
	},
	repState(state, { newState, dayIndex, setIndex, repIndex }) {
		const workout = state.workout,
			day = workout.days[dayIndex],
			set = day.sets[setIndex];

		set.reps.splice(repIndex, 1, newState);

		set.state = getResultState(set.reps);
		day.state = getResultState(day.sets.map((set) => set.state));
		workout.state = getResultState(workout.days.map((day) => day.state));
	},
	restTimer(state, newVal) {
		state.restTimer = newVal;
	},
	userId(state, newVal) {
		state.userId = newVal;
	},
	workout(state, newWorkout) {
		if (newWorkout.programHash === state.workout.programHash) {
			// given workout belongs to the same program as the current workout
			// -> it is applicable
			state.workout = newWorkout;
		}
	},
};
