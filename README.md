# train-vue

Training calculator for weightlifting.

[Demo](https://aheger.gitlab.io/train-vue/)

## Project Goals

- provide a way to calculate a weightlifting training program
- use Vue framework

## Build Setup

```bash
# install dependencies
npm install

# dev server with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
