const progression531 = [
	[{ count: 5, ratio: 0.65 }, { count: 5, ratio: 0.75 }, { count: 5, ratio: 0.85 }],
	[{ count: 3, ratio: 0.7 }, { count: 3, ratio: 0.8 }, { count: 3, ratio: 0.9 }],
	[{ count: 5, ratio: 0.75 }, { count: 3, ratio: 0.85 }, { count: 1, ratio: 0.95 }],
	[{ count: 5, ratio: 0.6 }, { count: 5, ratio: 0.6 }, { count: 5, ratio: 0.6 }],
];

const progression10s = [[
	{ count: 10, ratio: 0.5 },
	{ count: 10, ratio: 0.5 },
	{ count: 10, ratio: 0.5 },
	{ count: 10, ratio: 0.5 },
	{ count: 10, ratio: 0.5 },
]];

const template = [
	{
		name: 'Pull1',
		sets: [
			{ liftId: 'deadlift', progression: progression531 },
			{ liftId: 'pullup', progression: progression10s },
			{ liftId: 'lat-pull', progression: progression10s },
		],
	},
	{
		name: 'Push1',
		sets: [
			{ liftId: 'bench', progression: progression531 },
			{ liftId: 'ohp', progression: progression10s },
			{ liftId: 'dip', progression: progression10s },
		],
	},
	{
		name: 'Legs1',
		sets: [
			{ liftId: 'squat', progression: progression531 },
			{ liftId: 'front-squat', progression: progression10s },
			{ liftId: 'calf-raise', progression: progression10s },
		],
	},
	{
		name: 'Pull2',
		sets: [
			{ liftId: 'deadlift', progression: progression10s },
			{ liftId: 'pullup', progression: progression10s },
			{ liftId: 'lat-pull', progression: progression10s },
		],
	},
	{
		name: 'Push2',
		sets: [
			{ liftId: 'ohp', progression: progression531 },
			{ liftId: 'bench', progression: progression10s },
			{ liftId: 'dip', progression: progression10s },
		],
	},
	{
		name: 'Legs2',
		sets: [
			{ liftId: 'front-squat', progression: progression531 },
			{ liftId: 'squat', progression: progression10s },
			{ liftId: 'calf-raise', progression: progression10s },
		],
	},
];

let numWeeks = 0;
for (const tDay of template) {
	for (const tSet of tDay.sets) {
		if (numWeeks < tSet.progression.length) {
			numWeeks = tSet.progression.length;
		}
	}
}

const days = [];
for (let iWeek = 0; iWeek < numWeeks; iWeek ++) {
	for (const tDay of template) {
		const day = {
			name: `W${iWeek + 1}: ${tDay.name}`,
			sets: [],
		};

		for (const tSet of tDay.sets) {
			const progIx = iWeek % tSet.progression.length;

			const set = {
				liftId: tSet.liftId,
				reps: tSet.progression[progIx],
			}

			day.sets.push(set);
		}

		days.push(day);
	}
}

export default {
	days: days,
};
