export function immutAssign(obj, newProp) {
	return Object.assign({}, obj, newProp);
}

export function omit(obj, ...blacklisted) {
	return Object.keys(obj).reduce((newObj, key) => {
		if (!blacklisted.includes(key)) {
			Object.assign(newObj, { [key]: obj[key] });
		}
		return newObj;
	}, {});
}

export function pick(obj, ...picked) {
	return Object.keys(obj).reduce((newObj, key) => {
		if (picked.includes(key)) {
			Object.assign(newObj, { [key]: obj[key] });
		}
		return newObj;
	}, {});
}

export function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}
