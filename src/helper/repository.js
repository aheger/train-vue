/**
 * Create uuid
 */
function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

/**
 * Provides methods for persisting JSON data
 */
export default class {
	/**
	 * get the current repo id
	 *
	 * @returns {String}
	 */
	get id() {
		return this._id;
	}

	/**
	 * Initialize repo with given id.
	 * If no id is given a new unique repo id is created.
	 * If an id was already given in constructor it is returned.
	 *
	 * @param {String} [id] repo id
	 * @returns {Promise} resolves with given or created id
	 */
	init(id) {
		if (!id) {
			this._id = uuidv4();
		} else {
			this._id = id;
		}
		return Promise.resolve(this._id);
	}

	/**
	 * save repo data
	 *
	 * @returns {Promise}
	 */
	save(state) {
		localStorage.setItem(this._id, JSON.stringify(state));
		return Promise.resolve();
	}

	/**
	 * load repo data
	 *
	 * @returns {Promise}
	 */
	load() {
		const stateString = localStorage.getItem(this._id);

		if (stateString) {
			return Promise.resolve(JSON.parse(stateString));
		} else {
			return Promise.resolve({});
		}

	}
}
