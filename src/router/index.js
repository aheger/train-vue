import Vue from 'vue';
import Router from 'vue-router';
import DashPage from '@/pages/Dash';
import ErrorPage from '@/pages/Error';
import WorkoutPage from '@/pages/Workout';
import NotImplementedPage from '@/pages/NotImplemented';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/dash',
			name: 'Dash',
			component: DashPage,
		},
		{
			path: '/lifts',
			name: 'Lifts',
			component: NotImplementedPage,
		},
		{
			path: '/program',
			name: 'Program',
			component: NotImplementedPage,
		},
		{
			path: '/settings',
			name: 'Settings',
			component: NotImplementedPage,
		},
		{
			path: '/workout',
			name: 'Workout',
			component: WorkoutPage,
		},
		{
			path: '/error/:reason?',
			name: 'Error',
			component: ErrorPage,
		},
		{
			path: '*',
			redirect: { name: 'Dash' },
		},
	],
});
