import { progTypes } from '@/helper/constants';

export default [
	{
		matcher: /^(deadlift|jefferson-deadlift|snatch-deadlift|squat)$/,
		type: progTypes.PROG_TYPE_FIXED,
		value: 5,
	},
	{
		matcher: /^(bb-row|bench|curl|db-row|front-squat|incline-bench|ohp|oh-squat|one-leg-deadlift|recl-curl|reverse-lunge|split-squat|uh-db-bench)$/,
		type: progTypes.PROG_TYPE_FIXED,
		value: 2.5,
	},
	{
		matcher: /^(lat-raise|thread-needle)$/,
		type: progTypes.PROG_TYPE_FIXED,
		value: 1.25,
	},
];
