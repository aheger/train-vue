import { repStates, REST_INTERVAL } from '@/helper/constants';
import repo from './repo';
import router from '@/router';
import debounce from '@/helper/debounce';
import { pick } from '@/helper/object';
import { createWorkoutState, progressLifts } from './helper';
import DefaultProgress from './defaultProgress';

let restTimer = null,
	restEnd = null;

function getRepstate(state, params) {
	const { dayIndex, setIndex, repIndex } = params,
		workout = state.workout,
		day = workout.days[dayIndex],
		set = day.sets[setIndex];

	return set.reps[repIndex] || repStates.REP_STATE_NONE;
}

const debouncedSave = debounce((state, callback) => {
	repo.save(state).then(callback, callback);
}, 1000);

export default {
	/**
	 * Clear rest timer
	 */
	clearTimer({ commit }) {
		if (restTimer) {
			clearInterval(restTimer);
		}
		commit('restTimer', null);
	},
	/**
	 * Continue workout in a new session
	 */
	continueWorkout({ commit, getters }) {
		const cursor = getters.currentRep;

		if (cursor) {
			// workout is unfinished, set day index to first unfinished day
			commit('dayIndex', cursor.dayIndex);
		}
	},
	finishCycle({ commit, dispatch, state }) {
		const { lifts, workout, program } = state,
			progressed = progressLifts(lifts, workout, program, DefaultProgress);

		commit('dayIndex', 0);
		commit('lifts', progressed);
		commit('workout', createWorkoutState(program));

		return dispatch('save');
	},
	/**
	 * Initialize the store
	 */
	init({ commit, dispatch }) {
		var initialId = router.currentRoute.query.userId || sessionStorage.getItem('userId');

		commit('pushLoad');
		return repo
			.init(initialId)
			.then((storeId) => {
				commit('userId', storeId);
				sessionStorage.setItem('userId', storeId);
				router.replace({ query: { userId: storeId } });
			})
			.then(repo.load.bind(repo))
			.then((loaded) => {
				if (loaded.workout) {
					commit('workout', loaded.workout);
				}
				if (loaded.lifts) {
					commit('lifts', loaded.lifts);
				}
				return dispatch('continueWorkout');
			})
			.then(() => commit('popLoad'))
			.catch(() => {
				commit('popLoad');
				return Promise.reject();
			});
	},
	/**
	 * Set new workout day index
	 */
	setDay({ commit }, newIndex) {
		commit('dayIndex', newIndex);
	},
	/**
	 * Set new rep state
	 */
	setRepState({ commit, dispatch, state }, params) {
		if (getRepstate(state, params) === repStates.REP_STATE_NONE) {
			dispatch('startTimer');
		}
		commit('repState', params);

		return dispatch('save');
	},
	/**
	 * Save state to repo
	 */
	save({ state, commit }) {
		var saveState = pick(state, 'lifts', 'workout');

		commit('isSaving', true);

		debouncedSave(saveState, () => commit('isSaving', false));
	},
	/**
	 * Start rest times
	 */
	startTimer({ dispatch }) {
		dispatch('clearTimer');

		restEnd = new Date();
		restEnd.setSeconds(restEnd.getSeconds() + REST_INTERVAL);

		this.commit('restTimer', REST_INTERVAL);

		restTimer = setInterval(() => {
			let restLeft = Math.round((restEnd - new Date()) / 1000);

			if (restLeft <= 0) {
				return dispatch('clearTimer');
			}

			this.commit('restTimer', restLeft);
		}, 1000);
	},
	/**
	 * Switch current workout day by given delta
	 */
	switchDay({ commit, state }, delta) {
		commit('dayIndex', state.dayIndex + delta);
	},
	/**
	 * Switch rep state
	 */
	switchRepState({ dispatch, state }, params) {
		let newState;

		switch (getRepstate(state, params)) {
			case repStates.REP_STATE_NONE:
				newState = repStates.REP_STATE_DONE;
				break;
			case repStates.REP_STATE_DONE:
				newState = repStates.REP_STATE_FAILED;
				break;
			default:
				newState = repStates.REP_STATE_NONE;
				break;
		}

		return dispatch('setRepState', { ...params, newState });
	},
};
